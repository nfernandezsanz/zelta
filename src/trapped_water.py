def trapped_water(pillars):
    
    number_of_pillars = len(pillars)
    
    if number_of_pillars == 0:
        return 0
    
    if any(pillar < 0 for pillar in pillars):
        raise ValueError("All pillars must be non-negative integers.")
    
    # Create empty aux arrays for max height calculations
    max_height_to_left = [0] * number_of_pillars
    max_height_to_right = [0] * number_of_pillars

    # Calculate the maximum height to the left of each pillar
    max_height_to_left[0] = pillars[0]
    for i in range(1, number_of_pillars):
        max_height_to_left[i] = max(max_height_to_left[i - 1], pillars[i])

    # Calculate the maximum height to the right of each pillar
    max_height_to_right[number_of_pillars - 1] = pillars[number_of_pillars - 1]
    for i in range(number_of_pillars - 2, -1, -1):
        max_height_to_right[i] = max(max_height_to_right[i + 1], pillars[i])

    # Calculate the trapped water for each pillar by finding the minimum of the left and right max heights,
    # subtracting the height of the current pillar, and summing the results
    total_trapped_water = 0
    for i in range(number_of_pillars):
        
        # Calculate the minimum of left and right maximums
        min_of_max_heights = min(max_height_to_left[i], max_height_to_right[i])
        
        # Add trapped water only if it's greater than the current pillar height
        if min_of_max_heights > pillars[i]:
            total_trapped_water += min_of_max_heights - pillars[i]

    return total_trapped_water
