def merge_intervals(intervals):
    
    if not intervals:
        return []

    if any(start > end for start, end in intervals):
        raise ValueError("One or more intervals are invalid.")

    # Step 1: Sort the intervals by their starting values
    intervals.sort(key=lambda x: x[0])
    
    # Initialize merged intervals with the first interval
    merged_intervals = [intervals[0]]

    # Step 2: Traverse the sorted intervals and merge if necessary
    for i in range(1, len(intervals)):
        current_start, current_end = intervals[i]
        
        if(current_start > current_end):
            raise ValueError(f"Invalid interval: [{current_start}, {current_end}]")
        
        last_merged_start, last_merged_end = merged_intervals[-1]

        # Check for overlap
        if current_start <= last_merged_end:
            # Merge the intervals
            merged_intervals[-1] = [last_merged_start, max(last_merged_end, current_end)]
        else:
            # No overlap, so add the current interval to the result
            merged_intervals.append([current_start, current_end])

    return merged_intervals