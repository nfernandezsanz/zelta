# Zelta Challenge

This repository contains the code and tests for the Zelta challenge. The project includes two main modules, one for each exercise: **merge_intervals** and **trapped_water**, each with corresponding tests.

## Prerequisites

- [Python 3.8.10](https://www.python.org/downloads/release/python-3810/)

## Installation

Follow these steps to set up the project environment:

Clone the repository:

```
git clone https://github.com/username/zelta-challenge.git
cd zelta
```

Create a virtual environment (optional but recommended):

```
python3 -m venv venv
source venv/bin/activate  # On Windows, use `venv\Scripts\activate`
```

Install the required packages:

```
pip install -r requirements.txt
```

## Running the Code

You can execute the main modules as follows:

### Merge Intervals

```
python src/merge_intervals.py
```

### Trapped Water

```
python src/trapped_water.py
```

### Running the Tests

This project uses pytest for testing. You can run all the tests with the following command:

```
pytest
```