from src.trapped_water import trapped_water
import pytest

def test_empty_pillars():
    assert trapped_water([]) == 0

def test_single_pillar():
    assert trapped_water([5]) == 0

def test_no_trapped_water():
    assert trapped_water([3, 5, 5, 5, 3]) == 0

def test_all_same_height_pillars():
    assert trapped_water([5, 5, 5, 5, 5]) == 0

def test_sample_case():
    assert trapped_water([0, 8, 0, 0, 5, 0, 0, 10, 0, 0, 1, 1, 0, 3]) == 48

def test_alternating_heights():
    assert trapped_water([0, 2, 0, 2, 0, 2, 0]) == 4

def test_decreasing_and_increasing_pillars():
    assert trapped_water([5, 4, 3, 2, 1, 2, 3, 4, 5]) == 16

def test_mixed_case():
    assert trapped_water([4, 2, 0, 3, 2, 5]) == 9

def test_plain_surface():
    assert trapped_water([0, 0, 0, 0, 0, 0]) == 0
    
def test_negative_pillar():
    with pytest.raises(ValueError):
        trapped_water([4, 2, -1, 3, 2, 5])