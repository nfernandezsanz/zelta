import pytest
from src.merge_intervals import merge_intervals

def test_merge_empty_intervals():
    assert merge_intervals([]) == []

def test_merge_non_overlapping_intervals():
    intervals = [[1, 2], [3, 4], [5, 6]]
    assert merge_intervals(intervals) == [[1, 2], [3, 4], [5, 6]]

def test_merge_overlapping_intervals():
    intervals = [[1, 3], [2, 4], [5, 6]]
    assert merge_intervals(intervals) == [[1, 4], [5, 6]]

def test_merge_invalid_intervals():
    with pytest.raises(ValueError):
        merge_intervals([[1, 3], [4, 2], [5, 6]])